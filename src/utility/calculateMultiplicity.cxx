#include <iostream>                                                                                 
#include <vector>
#include <stdio.h>
#include <TString.h>
#include <utility>

#include <TObject.h>
#include <TF1.h>
#include <TFile.h>
#include <TString.h>
#include <TGraph.h>
#include <TStyle.h>
#include <TColor.h>
#include <TMath.h>
#include <TSystem.h>
#include <TSystemDirectory.h>
#include <TSystemFile.h>
#include <TList.h>
#include <TH1D.h>

#include "calculateMultiplicity.h"
#include "utilityFunctions.h"


ClassImp(MultClass);

MultClass::MultClass(){

  mult_th1d_ = NULL;
  data_set_max_ = -999;
  n_cent_bins_ = -999;
}

MultClass::~MultClass(){}


MultClass::MultClass(TH1D * mult_th1d, std::vector<double> n_centrality_percent, double data_set_max_percent){


  int num_mult_bins = mult_th1d->GetNbinsX();
  double bin_width = mult_th1d->GetBinWidth(num_mult_bins);

  if (data_set_max_percent > 1){
    data_set_max_ = data_set_max_percent*.01;
  }
  else {
    data_set_max_ = data_set_max_percent; 
  }

  mult_th1d_ = mult_th1d;
  

  if (n_centrality_percent.at(0) > 1 ) {
    for (std::vector<double>::iterator cent_bin_iter = n_centrality_percent.begin(); cent_bin_iter != n_centrality_percent.end(); ++cent_bin_iter){
      n_centrality_percent_.push_back( (.01) * (*cent_bin_iter) );
    }
  }
  else {
    n_centrality_percent_ = n_centrality_percent;
  }
  n_cent_bins_ = static_cast<int>(n_centrality_percent.size());
  MultValues();
}



void MultClass::MultValues(){


  std::vector< std::pair <double,double> > mult_percent_pair;

  int num_mult_bins = mult_th1d_->GetNbinsX();
  double n_entries = mult_th1d_->GetEntries();
  double percent =0;


  for( int i_bin =num_mult_bins; i_bin > 1; --i_bin){
    
    double mult = mult_th1d_->GetBinCenter(i_bin) + (mult_th1d_->GetBinWidth(i_bin)/2);
    percent += mult_th1d_->GetBinContent(i_bin) * (data_set_max_/n_entries);
    mult_percent_pair.push_back( make_pair( mult , percent ) );
  }

   for (std::vector<double>::iterator cent_bin_iter = n_centrality_percent_.begin(); cent_bin_iter != n_centrality_percent_.end(); ++cent_bin_iter){

    double n_mult(-999);
    double closest_diff(999);

    for (std::vector< std::pair<double,double> >::iterator pair_iter = mult_percent_pair.begin(); pair_iter != mult_percent_pair.end(); ++pair_iter){
    
      double diff = abs( (*cent_bin_iter) - (*pair_iter).second );
  
      if (diff < closest_diff){
	closest_diff = diff;
	n_mult = (*pair_iter).first;
      }
    }

    cout << " n_mult set " << n_mult << endl;
    n_mult_values_.push_back( n_mult );  

  } 
}

int MultClass::FindCentIndex(double mult){

  int centIndex(-999);
  double previous_mult(999);
  
   for (std::vector<double>::iterator i_mult_iter = n_mult_values_.begin(); i_mult_iter != n_mult_values_.end(); ++i_mult_iter){

     if ( mult < previous_mult   && mult  >= (*i_mult_iter)){
       centIndex = std::distance(n_mult_values_.begin(),i_mult_iter); 
     }
     previous_mult = (*i_mult_iter);
   }
  
  return centIndex;

} 


double MultClass::EventsByMaxPercent(){

  double nevents = mult_th1d_->GetEntries();
  return (nevents/data_set_max_ );

}

