#!/bin/bash

fileName=sim_test.list
outName=particle_out_cuts_test.root

root -b -q -l ../macros/RunFillDndyParticles.C\(\"$fileName\",\"$outName\"\)

echo "DONE!"
