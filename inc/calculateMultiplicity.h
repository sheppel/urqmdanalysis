#ifndef MULT_CLASS_H
#define MULT_CLASS_H

#include <iostream>                                                                                 
#include <vector>
#include <stdio.h>
#include <TString.h>
#include <utility>

#include <TObject.h>
#include <TF1.h>
#include <TFile.h>
#include <TString.h>
#include <TGraph.h>
#include <TStyle.h>
#include <TColor.h>
#include <TMath.h>
#include <TSystem.h>
#include <TSystemDirectory.h>
#include <TSystemFile.h>
#include <TList.h>
#include <TH1D.h>

class MultClass : public TObject {


 private:

  TH1D * mult_th1d_;
  double data_set_max_;
  int n_cent_bins_;
  std::vector<double> n_centrality_percent_;
  std::vector<double> n_mult_values_;

  void MultValues();

 public:

  MultClass();
  MultClass(TH1D * mult_th1d, std::vector<double> n_centrality_percent, double data_set_max_percent);
  virtual ~MultClass();
  int FindCentIndex(double mult);
  double EventsByMaxPercent();


  ClassDef(MultClass,3);

};




#endif
