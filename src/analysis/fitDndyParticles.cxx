#include <iostream>
#include <vector>
#include <fstream>
#include <stdio.h>
#include <string.h>
#include <sstream>
#include <cmath>

#include <TObject.h>
#include <TF1.h>
#include <TFile.h>
#include <TString.h>
#include <TGraph.h>
#include <TStyle.h>
#include <TColor.h>
#include <TMath.h>
#include <TSystem.h>
#include <TSystemDirectory.h>
#include <TSystemFile.h>
#include <TList.h>
#include <TH1D.h>

#include <TChain.h>
#include <TVector3.h>
#include <TLorentzVector.h>
#include <TString.h>
#include <TRandom3.h>
#include <TCanvas.h>

#include "ParticleDndyClass.h"
#include "utilityFunctions.h"
#include "calculateMultiplicity.h"

#include "URQMDEvent.h"
#include "URQMDTrack.h"
#include "URQMDReader.h"

void fitDndyParticles(TString fileName){
  
  TFile * file = new TFile(fileName,"OPEN");

  ParticleDndyClass *proton = (ParticleDndyClass*) file->Get("ProtonPlus");
  ParticleDndyClass *pionminus = (ParticleDndyClass*) file->Get("PionMinus");
  ParticleDndyClass *pionplus =  (ParticleDndyClass*) file->Get("PionPlus");
  ParticleDndyClass *lambda = (ParticleDndyClass*) file->Get("Lambda");
  ParticleDndyClass *kaonminus = (ParticleDndyClass*) file->Get("KaonMinus");
  ParticleDndyClass *kaonplus = (ParticleDndyClass*) file->Get("KaonPlus");

  TCanvas * fittingCan = new TCanvas("fittingCan","fittingCan",600,600);

  TF1 * kaonminus_gaussian = new TF1("","gaus(0)",-0.5,2);
  TF1 * kaonplus_gaussian = new TF1("","gaus(0)",-0.5,2);
  TF1 * pionminus_gaussian = new TF1("","gaus(0)",-0.5,2);
  TF1 * pionplus_gaussian = new TF1("","gaus(0)",-0.5,2);
  TF1 * proton_gaussian = new TF1("","gaus(0)+gaus(3)+gaus(6)",-0.5,2);
  TF1 * lambda_gaussian = new TF1("","gaus(0)+gaus(3)",-0.5,2);

  fittingCan->cd();

  kaonminus_gaussian->SetParameter(0,0.5);
  kaonminus_gaussian->SetParameter(1,0.4);
  kaonminus_gaussian->SetParameter(2,0.4);

  kaonminus->AddFunction(kaonminus_gaussian);
  kaonminus->FitAllCent("R");

  kaonplus_gaussian->SetParameter(0,2);
  kaonplus_gaussian->SetParameter(1,0.4);
  kaonplus_gaussian->SetParameter(2,0.4);

  kaonplus->AddFunction(kaonplus_gaussian);
  kaonplus->FitAllCent("R");

  pionminus_gaussian->SetParameter(0,18);
  pionminus_gaussian->SetParameter(1,0.4);
  pionminus_gaussian->SetParameter(2,0.4);

  pionminus->AddFunction(pionminus_gaussian);
  pionminus->FitAllCent("R");

  pionplus_gaussian->SetParameter(0,16);
  pionplus_gaussian->SetParameter(1,0.4);
  pionplus_gaussian->SetParameter(2,0.4);

  pionplus->AddFunction(pionplus_gaussian);
  pionplus->FitAllCent("R");

  proton_gaussian->SetParameter(0,120);
  proton_gaussian->SetParameter(1,0);
  proton_gaussian->SetParameter(2,0.1);
  proton_gaussian->SetParameter(3,40);
  proton_gaussian->SetParameter(4,0.4);
  proton_gaussian->SetParameter(5,0.4);
  proton_gaussian->SetParameter(6,5);
  proton_gaussian->SetParameter(7,1);
  proton_gaussian->SetParameter(8,0.1);

  proton->AddFunction(proton_gaussian);
  proton->FitAllCent("R");

  lambda_gaussian->SetParameter(0,1.4);
  lambda_gaussian->SetParameter(1,0.2);
  lambda_gaussian->SetParameter(2,0.4);
  lambda_gaussian->SetParameter(3,1.4);
  lambda_gaussian->SetParameter(4,0.6);
  lambda_gaussian->SetParameter(5,0.4);

  lambda->AddFunction(lambda_gaussian);
  lambda->FitAllCent("R");


 TCanvas * c1 = new TCanvas("lambda","lambda",800,600);
 c1->Divide(3,2);
 c1->cd(1);
 lambda->DrawHist(0,true);
 c1->cd(2);
 lambda->DrawHist(1,true);
 c1->cd(3);
 lambda->DrawHist(2,true);
 c1->cd(4);
 lambda->DrawHist(3,true);
 c1->cd(5);
 lambda->DrawHist(4,true);
 c1->cd(6);
 lambda->DrawHist(5,true);

 TCanvas * c2 = new TCanvas("proton","proton",800,600);
 c2->Divide(3,2);
 c2->cd(1);
 proton->DrawHist(0,true);
 c2->cd(2);
 proton->DrawHist(1,true);
 c2->cd(3);
 proton->DrawHist(2,true);
 c2->cd(4);
 proton->DrawHist(3,true);
 c2->cd(5);
 proton->DrawHist(4,true);
 c2->cd(6);
 proton->DrawHist(5,true);

 TCanvas * c3 = new TCanvas("pionminus","pionminus",800,600);
 c3->Divide(3,2);
 c3->cd(1);
 pionminus->DrawHist(0,true);
 c3->cd(2);
 pionminus->DrawHist(1,true);
 c3->cd(3);
 pionminus->DrawHist(2,true);
 c3->cd(4);
 pionminus->DrawHist(3,true);
 c3->cd(5);
 pionminus->DrawHist(4,true);
 c3->cd(6);
 pionminus->DrawHist(5,true);

 TCanvas * c5 = new TCanvas("pionplus","pionplus",800,600);
 c5->Divide(3,2);
 c5->cd(1);
 pionplus->DrawHist(0,true);
 c5->cd(2);
 pionplus->DrawHist(1,true);
 c5->cd(3);
 pionplus->DrawHist(2,true);
 c5->cd(4);
 pionplus->DrawHist(3,true);
 c5->cd(5);
 pionplus->DrawHist(4,true);
 c5->cd(6);
 pionplus->DrawHist(5,true);

 TCanvas * c6 = new TCanvas("kaonminus","kaonminus",800,600);
 c6->Divide(3,2);
 c6->cd(1);
 kaonminus->DrawHist(0,true);
 c6->cd(2);
 kaonminus->DrawHist(1,true);
 c6->cd(3);
 kaonminus->DrawHist(2,true);
 c6->cd(4);
 kaonminus->DrawHist(3,true);
 c6->cd(5);
 kaonminus->DrawHist(4,true);
 c6->cd(6);
 kaonminus->DrawHist(5,true);

 TCanvas * c7 = new TCanvas("kaonplus","kaonplus",800,600);
 c7->Divide(3,2);
 c7->cd(1);
 kaonplus->DrawHist(0,true);
 c7->cd(2);
 kaonplus->DrawHist(1,true);
 c7->cd(3);
 kaonplus->DrawHist(2,true);
 c7->cd(4);
 kaonplus->DrawHist(3,true);
 c7->cd(5);
 kaonplus->DrawHist(4,true);
 c7->cd(6);
 kaonplus->DrawHist(5,true);

}
