#include <iostream>
#include <vector>
#include <fstream>
#include <stdio.h>
#include <string.h>
#include <sstream>
#include <cmath>

#include <TObject.h>
#include <TF1.h>
#include <TFile.h>
#include <TString.h>
#include <TGraph.h>
#include <TStyle.h>
#include <TColor.h>
#include <TMath.h>
#include <TSystem.h>
#include <TSystemDirectory.h>
#include <TSystemFile.h>
#include <TList.h>
#include <TH1D.h>

#include <TChain.h>
#include <TVector3.h>
#include <TLorentzVector.h>
#include <TString.h>
#include <TRandom3.h>
#include <TCanvas.h>

#include "ParticleDndyClass.h"
#include "utilityFunctions.h"
#include "calculateMultiplicity.h"

#include "URQMDEvent.h"
#include "URQMDTrack.h"
#include "URQMDReader.h"

void fillDndyParticles(TString fileName, TString outfileName){

  TFile * outfile = new TFile(outfileName,"RECREATE");

  URQMDReader *reader = new URQMDReader(fileName);
  URQMDEvent *event = NULL;

  int nEvents(0);
  TH1D * multiplicity_th1d  = new TH1D("h_track_mult","Track Multiplicity; mult",500,0,500);

  for (long iEntry=0; iEntry<reader->GetEntries(); iEntry++){

    event = reader->GetEntry(iEntry);

    if (!event){
      cout <<"Unable to read event!" <<endl;
      return;
    }


    multiplicity_th1d->Fill(event->GetMultiplicity());

  }
  

  multiplicity_th1d->Draw();

  std::vector<double> cent_percent;
  cent_percent.push_back(5);
  cent_percent.push_back(10);
  cent_percent.push_back(15);
  cent_percent.push_back(20);
  cent_percent.push_back(25);
  cent_percent.push_back(30);
  
  MultClass * multclass = new MultClass(multiplicity_th1d, cent_percent,30);

  double events = multclass->EventsByMaxPercent();

  ParticleDndyClass *proton = new ParticleDndyClass("Proton",1,cent_percent,events);
  ParticleDndyClass *pionminus =  new ParticleDndyClass("Pion",-1,cent_percent,events);
  ParticleDndyClass *pionplus =  new ParticleDndyClass("Pion",1,cent_percent,events);
  ParticleDndyClass *lambda = new ParticleDndyClass("Lambda",0,cent_percent,events);
  ParticleDndyClass *kaonminus = new ParticleDndyClass("Kaon",-1,cent_percent,events);
  ParticleDndyClass *kaonplus = new ParticleDndyClass("Kaon",1,cent_percent,events);

  double beam_rapidity = -3.2;

  //Set Cuts
  proton->SetLowPtCut( Mt_m0ToPt(.2,proton->GetMass()) );
  pionplus->SetLowPtCut( Mt_m0ToPt(.2,pionplus->GetMass()) );
  pionminus->SetLowPtCut( Mt_m0ToPt(.2,pionplus->GetMass()) );

  std::vector <double> ybins;
  std::vector <double> pt_cuts;

  ybins.push_back(0.25/beam_rapidity);
  pt_cuts.push_back(0.5);
  ybins.push_back(0.50/beam_rapidity);
  pt_cuts.push_back(0.5);
  ybins.push_back(0.75/beam_rapidity);
  pt_cuts.push_back(0.5);
  ybins.push_back(1.0/beam_rapidity);
  pt_cuts.push_back(0.5);
  ybins.push_back(1.25/beam_rapidity);
  pt_cuts.push_back(0.75);
  ybins.push_back(1.50/beam_rapidity);
  pt_cuts.push_back(1.0);
  ybins.push_back(1.75/beam_rapidity);

  lambda->SetVariablePtCut(ybins,pt_cuts);

  proton->SetYOverYBeam(beam_rapidity);
  pionminus->SetYOverYBeam(beam_rapidity);
  pionplus->SetYOverYBeam(beam_rapidity);
  lambda->SetYOverYBeam(beam_rapidity);
  kaonminus->SetYOverYBeam(beam_rapidity);
  kaonplus->SetYOverYBeam(beam_rapidity);

for (long iEntry=0; iEntry<reader->GetEntries(); iEntry++){

    event = reader->GetEntry(iEntry);

    if (!event){
      cout <<"Unable to read event!" <<endl;
      return;
    }

    int cent_index = multclass->FindCentIndex(event->GetMultiplicity());

    for(long iTrack=0; iTrack<event->GetMultiplicity(); iTrack++){

      URQMDTrack * track = event->GetTrack(iTrack);
      double rapidity = track->GetRapidity();
      double geant_pid = track->GetGeantPid();
      double pt = track->GetPt();

      proton->Fill(rapidity/beam_rapidity,pt,cent_index,geant_pid);
      pionminus->Fill(rapidity/beam_rapidity,pt,cent_index,geant_pid);
      pionplus->Fill(rapidity/beam_rapidity,pt,cent_index,geant_pid);
      lambda->FillVariableCut(rapidity/beam_rapidity,pt,cent_index,geant_pid);
      kaonminus->Fill(rapidity/beam_rapidity,cent_index,geant_pid);
      kaonplus->Fill(rapidity/beam_rapidity,cent_index,geant_pid);
      
    }

  }
  
 outfile->cd();
 proton->Write(Form("%s",proton->GetChargeName().Data()),TObject::kOverwrite);
 lambda->Write(Form("%s",lambda->GetChargeName().Data()),TObject::kOverwrite);
 pionminus->Write(Form("%s",pionminus->GetChargeName().Data()),TObject::kOverwrite);
 pionplus->Write(Form("%s",pionplus->GetChargeName().Data()),TObject::kOverwrite);
 kaonminus->Write(Form("%s",kaonminus->GetChargeName().Data()),TObject::kOverwrite);
 kaonplus->Write(Form("%s",kaonplus->GetChargeName().Data()),TObject::kOverwrite);
 
}
