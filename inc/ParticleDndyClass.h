#ifndef PARTICLE_DNDY_CLASS_H
#define PARTICLE_DNDY_CLASS_H

//#ifdef private
//#undef private
//#endif

#include <iostream>
#include <vector>
#include <fstream>
#include <stdio.h>
#include <string.h>
#include <sstream>

#include <TObject.h>
#include <TF1.h>
#include <TFile.h>
#include <TString.h>
#include <TGraph.h>
#include <TStyle.h>
#include <TColor.h>
#include <TMath.h>
#include <TSystem.h>
#include <TSystemDirectory.h>
#include <TSystemFile.h>
#include <TList.h>
#include <TH1D.h>

class ParticleDndyClass : public TObject {

 private:

  TString particle_name_;
  TString particle_charge_name_; 
  float charge_;
  float mass_;
  int geant_pid_;
  int urqmd_pid_;
  bool is_normalized_;
  double n_events_;
  std::vector <TH1D*> dndy_th1d_;
  std::vector <TF1*> dndy_th1d_fit_;
  std::vector <double> centrality_percent_;
  std::vector <double> events_in_centbin_;
  std::vector <double> scale_;
  std::vector <double> ybins_;
  std::vector <double> pt_cuts_;
  void CalculateEventsInCentBin();  
  void SetScaling();
  void ScaleHist();
  double y_beam_;
  double low_pt_;

 public:

  ParticleDndyClass();
  ParticleDndyClass(TString particle_name, int charge, std::vector<double> centrality_percent, double n_events);
  virtual ~ParticleDndyClass();

  void DrawHist(int cent_index, bool normalize=true);
  void Fill(Float_t y, Int_t centIndex, Int_t geant_id);
  void Fill(Float_t y, Float_t pt, Int_t centIndex, Int_t geant_id);
  void FillVariableCut(Float_t y, Float_t pt, Int_t centIndex, Int_t geant_id);
  void SetYOverYBeam(double y_beam);
  TString GetChargeName(){return particle_charge_name_;}
  void AddFunction(TF1 * func);
  void FitAllCent(TString opts);
  void SetLowPtCut(double min_pt);
  void SetVariablePtCut(std::vector <double> ybins, std::vector <double> pt_cuts);
  Bool_t IsAbovePtCut(double y, double pt);

  double GetMass(){return mass_;}


  ClassDef(ParticleDndyClass,3);
};
  

#endif
