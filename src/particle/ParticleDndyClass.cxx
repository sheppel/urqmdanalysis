#include <iostream>
#include <vector>
#include <fstream>
#include <stdio.h>
#include <string.h>
#include <sstream>
#include <cmath>

#include <TObject.h>
#include <TF1.h>
#include <TFile.h>
#include <TString.h>
#include <TGraph.h>
#include <TStyle.h>
#include <TColor.h>
#include <TMath.h>
#include <TSystem.h>
#include <TSystemDirectory.h>
#include <TSystemFile.h>
#include <TList.h>
#include <TH1D.h>

#include "ParticleDndyClass.h"
#include "utilityFunctions.h"

ClassImp(ParticleDndyClass);

ParticleDndyClass::ParticleDndyClass(){
  
  particle_name_ = "";
  particle_charge_name_ = "";
  charge_= -999;
  mass_ = -999;
  geant_pid_ = -999;
  urqmd_pid_ =-999;
  n_events_=-999;
  y_beam_ = 1;
  is_normalized_=false;
  low_pt_ = 0;
}

ParticleDndyClass::~ParticleDndyClass(){

  //Clean up the vectors

  for (std::vector< TH1D* >::iterator it_clear = dndy_th1d_.begin() ; it_clear != dndy_th1d_.end(); ++it_clear)
    {
      delete (*it_clear);
    } 
  dndy_th1d_.clear();

  for (std::vector< TF1* >::iterator it_clear = dndy_th1d_fit_.begin() ; it_clear != dndy_th1d_fit_.end(); ++it_clear)
    {
      delete (*it_clear);
    } 
  dndy_th1d_fit_.clear();


}

ParticleDndyClass::ParticleDndyClass(TString particle_name, int charge, std::vector<double> centrality_percent,double n_events){

  is_normalized_=false;
  n_events_=n_events;
  particle_name_  = particle_name.Data();
  particle_charge_name_ = GetParticleChargeName(particle_name,charge);
  charge_ = charge;
  mass_   = GetParticleMass(particle_name_);
  geant_pid_ = GetGeantPid(particle_charge_name_);
  urqmd_pid_ = GetUrqmdPid(particle_charge_name_);
  centrality_percent_ = centrality_percent;
  low_pt_ = 0;
  y_beam_ = 1;

  int n_cent_bins = static_cast<int>(centrality_percent.size());
  
  for (int i_cent = 0; i_cent < n_cent_bins; i_cent++){
    
    TString dndy_th1d_name = TString::Format("%s_centBin%d",particle_charge_name_.Data(),i_cent);
    TString dndy_th1d_title = TString::Format("%s dNdy centrality [%d,%d] ; y ; dN/dy",particle_charge_name_.Data(), i_cent == 0 ? 0 : (int)centrality_percent[i_cent-1], (int)centrality_percent[i_cent] );
    
    dndy_th1d_.push_back( new TH1D(dndy_th1d_name,dndy_th1d_title,100,-0.5,1.5) );
    scale_.push_back(1);    
  }
  
  CalculateEventsInCentBin();
  SetScaling();


}

void ParticleDndyClass::Fill(Float_t y, Int_t centIndex,Int_t geant_id){

  int dndy_th1d_size = static_cast<int>(dndy_th1d_.size());

  if (0 > centIndex || centIndex >= dndy_th1d_size){

    return;
  } 
  

  if (geant_id == geant_pid_){
    dndy_th1d_.at(centIndex)->Fill(y);
  }

}

void ParticleDndyClass::Fill(Float_t y,Float_t pt, Int_t centIndex,Int_t geant_id){

  int dndy_th1d_size = static_cast<int>(dndy_th1d_.size());

  if (0 > centIndex || centIndex >= dndy_th1d_size){

    return;
  } 
  

  if (geant_id == geant_pid_){

    if(pt > low_pt_) dndy_th1d_.at(centIndex)->Fill(y);
  }

}

void ParticleDndyClass::FillVariableCut(Float_t y,Float_t pt, Int_t centIndex,Int_t geant_id){

  int dndy_th1d_size = static_cast<int>(dndy_th1d_.size());

  if (0 > centIndex || centIndex >= dndy_th1d_size){

    return;
  } 
  

  if (geant_id == geant_pid_){

    if (IsAbovePtCut(y,pt)) dndy_th1d_.at(centIndex)->Fill(y);

  }

}


void ParticleDndyClass::SetScaling(){

  int n_cent_bins = static_cast<int>(centrality_percent_.size());

  for (int i_cent = 0; i_cent < n_cent_bins; i_cent++){
  scale_.at(i_cent) = ( 1/events_in_centbin_.at(i_cent)) * ( 1/dndy_th1d_.at(i_cent)->GetBinWidth(1) ) * (1/y_beam_ );
  }

}

void ParticleDndyClass::ScaleHist(){

  SetScaling();

  if (is_normalized_ == false){
 
    cout << "Scaling hist" << endl;
    int n_cent_bins = static_cast<int>(centrality_percent_.size());
    
    for (int i_cent = 0; i_cent < n_cent_bins; i_cent++){
      
      dndy_th1d_.at(i_cent)->Scale( scale_.at(i_cent) );
      
      cout << "Scale= " << scale_.at(i_cent) << endl;

    }
  }
  is_normalized_=true;
}


void ParticleDndyClass::CalculateEventsInCentBin(){

  double previous_percent = 0;
  for (std::vector<double>::iterator events_iter = centrality_percent_.begin(); events_iter != centrality_percent_.end(); events_iter++){
    double current_percent = (*events_iter);
    if (current_percent > 1) current_percent = current_percent*.01;
    
    events_in_centbin_.push_back( (current_percent - previous_percent) * n_events_ );
    previous_percent = current_percent;
  }
  
}

void ParticleDndyClass::DrawHist(int cent_index, bool normalize){
  
  if (normalize==true){
    if(n_events_ < 0 && is_normalized_ ==false ) {
     
      cout << "Hist not normalized and no event number set... nothing drawn" << endl;
      return;
    }
    
    if(is_normalized_ ==true ) {
      
      dndy_th1d_.at(cent_index)->Draw();
      
    }

    if(n_events_ > 0 && is_normalized_ ==false ) {
      
      dndy_th1d_.at(cent_index)->Scale( 1/events_in_centbin_.at(cent_index) );
      dndy_th1d_.at(cent_index)->Scale( 1/dndy_th1d_.at(cent_index)->GetBinWidth(1) );
      dndy_th1d_.at(cent_index)->Scale( 1/y_beam_ );
      dndy_th1d_.at(cent_index)->Draw();

      scale_.at(cent_index) = ( 1/events_in_centbin_.at(cent_index)) * ( 1/dndy_th1d_.at(cent_index)->GetBinWidth(1) ) * (1/y_beam_ );
      
    }
  }

  if (normalize==false){

    dndy_th1d_.at(cent_index)->Draw();

  }
}

void ParticleDndyClass::SetYOverYBeam(double y_beam){

  y_beam_ = abs(y_beam);

  int n_cent_bins = static_cast<int>(centrality_percent_.size());  
  for (int i_cent = 0; i_cent < n_cent_bins; i_cent++){

   TString dndy_th1d_title = TString::Format("%s dNdy centrality [%d,%d] ; y_{Lab}/y_{Beam}; dN/dy",particle_charge_name_.Data(), i_cent == 0 ? 0 : (int)centrality_percent_[i_cent-1], (int)centrality_percent_[i_cent] );

   dndy_th1d_.at(i_cent)->SetTitle(dndy_th1d_title);

  }

  SetScaling();

}

void ParticleDndyClass::AddFunction(TF1 * func){

  ScaleHist();
  int n_cent_bins = static_cast<int>(centrality_percent_.size());  
  for (int i_cent = 0; i_cent < n_cent_bins; i_cent++){
    
    TF1 * temp_func = func;
    TString dndy_func_name = TString::Format("%s_cent%02d_fit",particle_charge_name_.Data(), i_cent);
    temp_func->SetName(dndy_func_name.Data());
    cout << "Func Name: " << dndy_func_name << endl;
    dndy_th1d_fit_.push_back(temp_func);
 
  }

}


void ParticleDndyClass::FitAllCent(TString opts){

 int n_cent_bins = static_cast<int>(centrality_percent_.size());  
  for (int i_cent = 0; i_cent < n_cent_bins; i_cent++){
 
    dndy_th1d_.at(i_cent)->Fit(dndy_th1d_fit_.at(i_cent),opts.Data());

    
  }

}

void ParticleDndyClass::SetLowPtCut(double min_pt){

  low_pt_ = min_pt;

}

void ParticleDndyClass::SetVariablePtCut(std::vector <double> ybins, std::vector <double> pt_cuts){

  int n_y_bins = static_cast<int>(ybins_.size());
  int cut_bins = static_cast<int>(pt_cuts_.size());
  
  if ( (n_y_bins - 1) == cut_bins ) { cout << "ERROR:: ybins[].size() should equal pt_cuts[].size()!!!" << endl; return; }

  ybins_ = ybins;
  pt_cuts_ = pt_cuts;

}

Bool_t ParticleDndyClass::IsAbovePtCut(double y, double pt){

  int y_bin = -999;
  
  int n_y_bins = static_cast<int>(ybins_.size());
  
  if(n_y_bins < 1){
    cout << "PT cut vector is not set!!!" << endl;
    return false;
  }
  
  for( int i_bin = 0; i_bin < (n_y_bins-1); i_bin++){
    if (ybins_[i_bin] < y && ybins_[i_bin+1] > y) y_bin = i_bin; 
  }

  if (y_bin == -999){ return false; }
  
  if ( pt > pt_cuts_[y_bin] ) return true; 


}
