#ifndef UTILITY_FUNCTIONS_H
#define UTILITY_FUNCTIONS_H

#include <iostream>                                                                                 
#include <vector>
#include <stdio.h>
#include <TString.h>




  float GetParticleMass(TString particle_name);
  TString GetParticleChargeName(TString particle_name, Int_t charge);
  TString GetChargeName(Int_t charge);
  Int_t GetGeantPid(TString particle_name);
  Int_t GetUrqmdPid(TString particle_name);
  Int_t GeantPidToUrqmdPid(Int_t geant_pid);
  double Mt_m0ToPt(double mt_m0, double mass); 



#endif
