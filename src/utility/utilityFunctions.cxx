
#include <iostream>                                                                                 
#include <vector>
#include <stdio.h>
#include <TString.h>

#include "utilityFunctions.h"


float GetParticleMass(TString particle_name){
  
  Int_t geant_pid = GetGeantPid(particle_name);
  
  Float_t particle_mass_array[] = { 0.0,       // ReducedMassNucleon, #0
				    0.0,       // Gamma,              #1
				    0.000511,  // ElectronPlus,       #2
				    0.000511,  // ElectronMinus,      #3
				    0.0,       // Neutrino,           #4
				    0.1057,    // MuonPlus,           #5
				    0.1057,    // MuonMinus,          #6
				    0.1350,    // Pion0,              #7
				    0.1396,    // PionPlus,           #8
				    0.1396,    // PionMinus,          #9
				    0.4977,    // Kaon0Long,          #10
				    0.4937,    // KaonPlus,           #11
				    0.4937,    // KaonMinus,          #12
				    0.9396,    // Neutron,            #13
				    0.9383,    // ProtonPlus,         #14
				    0.9383,    // ProtonMinus,        #15
				    0.4977,    // Kaon0Short,         #16
				    0.5475,    // Eta,                #17
				    1.116,     // Lambda,             #18
				    1.189,     // SigmaPlus,          #19
				    1.193,     // Sigma0,	        #20
				    1.197,     // SigmaMinus,         #21
				    1.315,     // Xi0,                #22
				    1.321,     // XiMinus,            #23
				    1.672      // OmegaMinus}         #24
  };
  
  float pmass = particle_mass_array[geant_pid];
  
  return pmass;
}



TString GetParticleChargeName(TString particle_name,Int_t charge){
  
  TString particle_charge_name = TString::Format("%s%s",particle_name.Data(),GetChargeName(charge).Data());
  
  return particle_charge_name;
}


TString GetChargeName(Int_t charge){
  
  TString charge_name = "";
  switch(charge) 
    {
      
    case 1:
      charge_name = "Plus";
      break;
      
    case -1:
      charge_name = "Minus";
      break;
      
    default:
      break;
    }
  
  return charge_name;
}

Int_t GetGeantPid(TString particle_name){
  
  int geant_pid(-999);
  TString geant_pid_array[] = {"ReducedMassNucleon", //0
			       "Gamma",              //1
			       "ElectronPlus",       //2
			       "ElectronMinus",      //3
			       "Neutrino",           //4
			       "MuonPlus",           //5
			       "MuonMinus",          //6
			       "Pion0",              //7
			       "PionPlus",           //8
			       "PionMinus",          //9
			       "Kaon0Long",          //10
			       "KaonPlus",           //11
			       "KaonMinus",          //12
			       "Neutron",            //13
			       "ProtonPlus",         //14
			       "ProtonMinus",        //15
			       "Kaon0Short",         //16
			       "Eta",                //17
			       "Lambda",             //18
			       "SigmaPlus",          //19
			       "Sigma0",	     //20
			       "SigmaMinus",         //21
			       "Xi0",                //22
			       "XiMinus",            //23
			       "OmegaMinus"};        //24
  
  for (int i_particle=0; i_particle <= 24; i_particle++){
    if ( !particle_name.CompareTo(geant_pid_array[i_particle]) ){
      geant_pid = i_particle;
      continue;
    }
  }
  
  return geant_pid;
}

Int_t GetUrqmdPid(TString particle_name){
  
  Int_t geant_pid = GetGeantPid(particle_name);
  Int_t urqmd_pid = GeantPidToUrqmdPid(geant_pid);
  
  return urqmd_pid;
}


Int_t GeantPidToUrqmdPid(Int_t geant_pid){
  
  Int_t urqmd_pid(-999);
  
  switch(geant_pid){
    //Pions
  case 7: urqmd_pid = 101; break;
  case 8: urqmd_pid = 101; break;
  case 9: urqmd_pid = 101; break;
    
    //Eta
  case 17: urqmd_pid = 102; break;
    
    //Kaons
  case 16: urqmd_pid = 106; break;
  case 10: urqmd_pid = 106; break;
  case 11: urqmd_pid = 106; break;
  case 12: urqmd_pid = -106; break;
    
    //Nucleons
  case 14: urqmd_pid = 1; break;
  case 15: urqmd_pid = -1; break;
  case 13: urqmd_pid = 1; break;
  case 25: urqmd_pid = -1; break;
    
    //Sigma
  case 19: urqmd_pid = 40; break;
  case 21: urqmd_pid = 40; break;
  case 29: urqmd_pid = -40; break;
  case 27: urqmd_pid = -40; break;
  case 20: urqmd_pid = 40; break;
  case 28: urqmd_pid = -40; break;
    
    //Xi
  case 22: urqmd_pid = 49; break;
  case 30: urqmd_pid = -49; break;
  case 23: urqmd_pid = 49; break;
  case 31: urqmd_pid = -49; break;
    
    //Lambda
  case 18: urqmd_pid = 27; break;  
  case 26: urqmd_pid = -27; break;  
    
    //Omega
  case 24: urqmd_pid = 55; break;  
  case 32: urqmd_pid = -55; break;  
    
  default: urqmd_pid = -999; break;
    
    return urqmd_pid;
  }

}

double Mt_m0ToPt(double mt_m0, double mass){

  return sqrt(pow(((mt_m0) + mass),2) - pow(mass,2));
}




